package blackjack;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class DummySteps {
    private int first;
    private int sum;

    @Given("first number is {string}")
    public void firstNumberIs(String firstNumber) {
        this.first = Integer.parseInt(firstNumber);
    }

    @When("we adding {string} to first number")
    public void weAddingToFirstNumber(String secondNumber) {
        this.sum = first + Integer.parseInt(secondNumber);
    }

    @Then("sum is {string}")
    public void sumIs(String sum) {
        Assert.assertEquals(Integer.parseInt(sum), this.sum);
    }
}
